import React from 'react';
import { ListRenderItemInfo, View, Text } from 'react-native';
import { Input, Layout, List, StyleService, useStyleSheet,TopNavigation,Divider,TopNavigationAction,Icon } from '@ui-kitten/components';
import { MessageItem } from './extra/message-item.component';
import { SearchIcon,MenuIcon } from './extra/icons';
import { Message } from './extra/data';

const initialMessages: Message[] = [
  Message.howAreYou(),
  Message.canYouSend(),
  Message.noProblem(),
];


export default ({ navigation }): React.ReactElement => {
  const DrawerOpen = () => {
    navigation.openDrawer();
  };

  const DrawerAction = () => (
    <TopNavigationAction icon={MenuIcon} onPress={DrawerOpen}/>
  );
  const styles = useStyleSheet(themedStyles);
  const [searchQuery, setSearchQuery] = React.useState<string>();

  const onItemPress = (index: number): void => {
    navigation && navigation.navigate('Chat2');
  };

  const renderItem = (info: ListRenderItemInfo<Message>): React.ReactElement => (
    <MessageItem
      style={styles.item}
      message={info.item}
      onPress={onItemPress}
    />
  );

  const renderHeader = (): React.ReactElement => (
    <Layout>
      <TopNavigation title='CONVERSATION' alignment='center' accessoryLeft={DrawerAction}/>
      <Divider/>
      <Input
        placeholder='Search'
        value={searchQuery}
        accessoryRight={SearchIcon}
      />
    </Layout>
  );

  return (
    <List
    style={styles.list}
    data={initialMessages}
    renderItem={renderItem}
    ListHeaderComponent={renderHeader}
  />
 
  );
};

const themedStyles = StyleService.create({
  list: {
    flex: 1,
  },
  header: {
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: 8,
  },
  item: {
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: 'background-basic-color-3',
  },
});
