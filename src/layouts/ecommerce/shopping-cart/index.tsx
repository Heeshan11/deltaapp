import React from 'react';
import { ListRenderItemInfo ,View,ScrollView} from 'react-native';
import { Button, Layout, List, StyleService, Text, useStyleSheet,TopNavigationAction,Icon,TopNavigation,Divider } from '@ui-kitten/components';
import { CartItem } from './extra/cart-item.component';
import { Product } from './extra/data';
import {  } from 'react-native-gesture-handler';


const BackIcon = (props) => (
  <Icon {...props} name='arrow-ios-back-outline' />
)
const initialProducts: Product[] = [
  Product.pinkChair(),
  Product.blackLamp(),
];

export default ({navigation}): React.ReactElement => {
  const navigateBack = () => {
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack}/>
  );
  const styles = useStyleSheet(themedStyle);
  const [products, setProducts] = React.useState<Product[]>(initialProducts);

  const totalCost = (): number => {
    return products.reduce((acc: number, product: Product): number => acc + product.totalPrice, 0);
  };

  const onItemRemove = (product: Product, index: number): void => {
    products.splice(index, 1);
    setProducts([...products]);
  };

  const onItemChange = (product: Product, index: number): void => {
    products[index] = product;
    setProducts([...products]);
  };

  const renderFooter = (): React.ReactElement => (
    <Layout style={styles.footer}>
      <Text category='h5'>Total Cost:</Text>
      <Text category='h5'>{`$${totalCost()}`}</Text>
    </Layout>
  );

  const renderProductItem = (info: ListRenderItemInfo<Product>): React.ReactElement => (
    <CartItem
      style={styles.item}
      index={info.index}
      product={info.item}
      onProductChange={onItemChange}
      onRemove={onItemRemove}
    />
  );

  return (
  <View>
    <TopNavigation title="Shopping Cart" alignment='center' accessoryLeft={BackAction}/>
    <Divider/>
    <ScrollView>
    <Layout
      style={styles.container}
      level='2'>
      <List
        data={products}
        renderItem={renderProductItem}
        ListFooterComponent={renderFooter}
      />
      <Button
        style={styles.checkoutButton}
        size='giant'>
        CHECKOUT
      </Button>
    </Layout>
  </ScrollView> 
</View>
   
  );
};

const themedStyle = StyleService.create({
  container: {
    flex: 1,
  },
  item: {
    borderBottomWidth: 1,
    borderBottomColor: 'background-basic-color-3',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 0.5,
    paddingVertical: 28,
    paddingHorizontal: 16,
  },
  checkoutButton: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
});

