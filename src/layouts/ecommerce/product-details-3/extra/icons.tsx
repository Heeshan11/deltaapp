import React from 'react';
import { ImageStyle } from 'react-native';
import { Button, Icon,IconElement } from '@ui-kitten/components';

export const HeartIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='heart-outline'/>
);

export const MessageCircleIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='message-circle-outline'/>
);

export const MoreHorizontalIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='more-horizontal'/>
);
export const CartIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='shopping-cart'/>
)
