import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { Tab, TabBar,TopNavigation,Divider,TopNavigationAction,Icon} from '@ui-kitten/components';
import { ProductListScreen } from './product-list.component';
import { SafeAreaView, Text,Image, View, StatusBar } from 'react-native';
import { Title,Caption } from 'react-native-paper';

const MenuIcon = (props) => (
  <Icon {...props} name='menu' />
);
const ProductsTabBar = ({ navigation, state }): React.ReactElement => {
  
  const DrawerOpen = () => {
    navigation.openDrawer();
  };

  const DrawerAction = () => (
    <TopNavigationAction icon={MenuIcon} onPress={DrawerOpen}/>
  );
  const onTabSelect = (index: number): void => {
    navigation.navigate(state.routeNames[index]);
  };

  const renderTab = (route: string): React.ReactElement => (
    <Tab
      key={route}
      title={route.toUpperCase()}
    />
  );
const logo =()=>(
  <Title style={{color:"#e40004"}}>Delta.Lk</Title>
)
  return (
    <SafeAreaView>
      <StatusBar/>
      <TopNavigation title={logo} alignment='center' accessoryLeft={DrawerAction}/>
      <Divider/>
      
      <TabBar
        selectedIndex={state.index}
        onSelect={onTabSelect}>
        {state.routeNames.map(renderTab)}
      </TabBar>
    </SafeAreaView>
  );
};

const TopTabs = createMaterialTopTabNavigator();

export default (): React.ReactElement => (
  <TopTabs.Navigator tabBar={(props) => <ProductsTabBar {...props}/>}>
    <TopTabs.Screen name='All' component={ProductListScreen}/>
    <TopTabs.Screen name='Furniture' component={ProductListScreen}/>
    <TopTabs.Screen name='Lighting' component={ProductListScreen}/>
  </TopTabs.Navigator>
);
