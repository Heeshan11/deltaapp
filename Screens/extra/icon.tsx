import React from 'react';
import { ImageStyle } from 'react-native';
import { Icon, IconElement } from '@ui-kitten/components';


export const chatIcon = (style: ImageStyle): IconElement => (
    <Icon {...style} name='email'/>
  );
  
  export const profileIcon = (style: ImageStyle): IconElement => (
    <Icon {...style} name='person-outline'/>
  );
  
  export const homeIcon = (style: ImageStyle): IconElement => (
    <Icon {...style} name='Home-outline'/>
  );
  
 