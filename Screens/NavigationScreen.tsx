import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Drawer, DrawerItem, Layout, Text, IndexPath } from '@ui-kitten/components';
import {DrawerContents} from './DrawerContents'
import {BottomTabNavigator} from './BottomTabs'
import shoppingCart from '../src/layouts/ecommerce/shopping-cart';
import signIn2 from '../src/layouts/auth/sign-in-2';
import signUp2 from '../src/layouts/auth/sign-up-2';
import productDetails3 from '../src/layouts/ecommerce/product-details-3';
import payment from '../src/layouts/ecommerce/payment';
import chat2 from '../src/layouts/messaging/chat-2';
const { Navigator, Screen } = createDrawerNavigator();

export const DrawerNavigator = () => (
  <Navigator drawerContent={props => <DrawerContents {...props} />}>
    <Screen name='HomeDrawer' component={BottomTabNavigator}/>
    <Screen name='ProductDetails3' component={productDetails3}/>
    <Screen name='Chat2' component={chat2}/>
    <Screen name="ShoppingCart" component={shoppingCart}/>
    <Screen name="SignInScreen" component={signIn2}/>
    <Screen name="SignUpScreen" component={signUp2}/>
    <Screen name="PaymentScreen" component={payment}/>
  </Navigator>
);

export const AppNavigator = () => (
  <NavigationContainer>
    <DrawerNavigator/>
  </NavigationContainer>
);