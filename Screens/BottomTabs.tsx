import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigation, BottomNavigationTab, Layout, Text ,Icon} from '@ui-kitten/components';
import ProductList from '../src/layouts/ecommerce/product-list/index';
import chatList from '../src/layouts/messaging/conversation-list'
import Profile from '../src/layouts/social/profile-6'
// import {homeIcon,chatIcon,profileIcon} from './extra/icon'

const homeIcon = (props:any) => (
  <Icon {...props} name='home-outline'/>
);

const chatIcon = (props:any) => (
  <Icon {...props} name='message-square-outline'/>
);

const profileIcon = (props:any) => (
  <Icon {...props} name='person-outline'/>
);
const { Navigator, Screen } = createBottomTabNavigator();

const BottomTabBar = ({ navigation, state }:any) => (
  <BottomNavigation
    selectedIndex={state.index}
    onSelect={index => navigation.navigate(state.routeNames[index])}>
    <BottomNavigationTab title='Home' icon={homeIcon}/>
    <BottomNavigationTab title='Chat' icon={chatIcon}/>
    <BottomNavigationTab title='Profile' icon={profileIcon}/>
  </BottomNavigation>
);

export const BottomTabNavigator = () => (
  <Navigator tabBar={props => <BottomTabBar {...props} />}>
    <Screen name='Home' component={ProductList}/>
    <Screen name='Chat' component={chatList}/>
    <Screen name='Profile' component={Profile}/>
  </Navigator>
);

