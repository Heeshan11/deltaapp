import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerContentScrollView,DrawerItem } from '@react-navigation/drawer';
import {  Layout, IndexPath } from '@ui-kitten/components';
import { View,Text, Image} from 'react-native'
import {Drawer, Divider} from 'react-native-paper'
const { Navigator, Screen } = createDrawerNavigator();

export const DrawerContents = (props:any) => {
    return( 
        <View style={{flex:1}}>
           <Text style={{textAlign:'center',height:55,justifyContent:'center',paddingVertical:15,backgroundColor:"#e40004",fontSize:19,color:'#fff',fontWeight:'bold'}}>Delta.Lk</Text>
           <Divider/>
            <DrawerContentScrollView {...props}>
               
                <Drawer.Section>
                    <DrawerItem label="Home"  onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="Cosmetics" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="Home Appliances" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="Electronic Accessories" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="Sports & Outdoor" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="Meat Shop" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="For Women" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="For men" onPress={()=>{props.navigation.navigate("Home")}}/>
                    <DrawerItem label="Kids" onPress={()=>{props.navigation.navigate("Home")}}/>
                   
                    <DrawerItem label="Sign In" onPress={()=>{props.navigation.navigate("SignInScreen")}}/>
                    <DrawerItem label="Sign Up" onPress={()=>{props.navigation.navigate("SignUpScreen")}}/>
                </Drawer.Section>
            </DrawerContentScrollView>
        </View>
    )
};
